const getSum = (str1, str2) => {
  // add your implementation below
  const isString = (str) => typeof str === 'string';
  if (!isString(str1) || !isString(str2)) {
    return false;
  }
  const num1 = Number(str1);
  const num2 = Number(str2);

  if (isNaN(num1) || isNaN(num2)) {
    return false;
  }

  return String(num1 + num2);
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  // add your implementation below
  let postsCounter = 0;
  let commentsCounter = 0;

  listOfPosts.forEach(({ author, comments }) => {
    postsCounter = author === authorName ? postsCounter + 1 : postsCounter;
    if (comments) {
      comments.map((comment) => commentsCounter = comment.author === authorName ? commentsCounter + 1 : commentsCounter)
    }
  })
  return `Post:${postsCounter},comments:${commentsCounter}`;
};

const tickets=(people)=> {
  // add your implementation below
  let answer = 'YES';
  const price = 25;

  people.reduce((acc, cash) => {
    if (cash === price) {
      return acc + price;
    }
    const change = cash - price;
    return change <= acc ? acc + price : answer = 'NO';  
  }, 0)
  return answer;
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
